# Import Libraries
import pandas as pd
import os
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib
import numpy as np
import scipy.stats
import os


# Read Dataframe from relative path
input1 = "../Data/part1.csv"

Mydataframe = pd.read_csv(os.path.abspath(input1), sep=",")
print(Mydataframe)

# Create a new column called TOTAL_COST
Mydataframe['TOTAL_COST'] = Mydataframe.loc[0: 36500, ["THIRD_COURSE", "SECOND_COURSE", "FIRST_COURSE"]].sum(axis=1)
print(Mydataframe['TOTAL_COST'])
Mydataframe.to_csv(input1, index=False)

# Make a plot of the distribution of the cost at each time.
x1 = Mydataframe.loc[Mydataframe.TIME == 'LUNCH', 'TOTAL_COST']
x2 = Mydataframe.loc[Mydataframe.TIME == 'DINNER', 'TOTAL_COST']
minimum = (Mydataframe['TOTAL_COST'].max())
maximum = (Mydataframe['TOTAL_COST'].min())
minx1 = min(min(x1), min(x2))
max2 = max(max(x1), max(x2))
print(minx1)
print(max2)
#
kwargs = dict(alpha=0.7, bins=10)
#
plt.hist(x1, **kwargs, color='b', label='LUNCH')
plt.hist(x2, **kwargs, color='r', label='DINNER')
plt.gca().set(title='Distribution of cost for each time', ylabel='Frequency(number of client)', xlabel='cost')
plt.xlim(maximum, minimum)
plt.legend(labels=['DINNER', 'LUNCH'])
plt.savefig(os.path.abspath('../Results/Cost Distribution at each time.png'))
plt.show()



#Make a bar plot for each course
x1 = Mydataframe['FIRST_COURSE']
x2 = Mydataframe['SECOND_COURSE']
x3 = Mydataframe['THIRD_COURSE']


plt.bar('FIRST_COURSE',x1, color='g', label='FIRST_COURSE')
plt.bar('SECOND_COURSE',x2, color='b', label='SECOND_COURSE')
plt.bar('THIRD_COURSE',x3, color='r', label='THIRD_COURSE')
plt.gca().set(title='cost per course', ylabel='cost', xlabel='Course')
plt.savefig(os.path.abspath('../Results/Course Bar Plot.png'))
plt.show()


#Determining the costs of drinks for each course
firstC = Mydataframe['FIRST_COURSE']
secondC = Mydataframe['SECOND_COURSE']
thirdC = Mydataframe['THIRD_COURSE']

first_drink = []
first_food = []
for i in firstC:
    if (i == 0.0):
        first_drink.append(float(i))
        first_food.append(float(i))
    elif (i == 3.0):
        first_drink.append(float(i - 3.0))
        first_food.append(float(i))
    elif (15 > i > 3):
        first_drink.append(float(i - 3.0))
        first_food.append(float(3.0))
    elif (15 <= i < 20):
        first_drink.append(float(i - 15.0))
        first_food.append(float(15.0))
    else:
        first_drink.append(float(i - 20.0))
        first_food.append(float(20.0))

df_firstD = pd.DataFrame(first_drink, columns=['first_drink'])
df_firstF = pd.DataFrame(first_food, columns=['first_food'])

Mydataframe['FIRST_DRINK'] = df_firstD.loc[0: 36500]
Mydataframe['FIRST_FOOD'] = df_firstF.loc[0: 36500]

second_drink = []
second_food = []
for i in secondC:
    if (i == 0.0):
        second_drink.append(float(i))
        second_food.append(float(i))
    elif (i == 9.0):
        second_drink.append(float(i - 9.0))
        second_food.append(float(i))
    elif (20 > i > 9):
        second_drink.append(float(i - 9.0))
        second_food.append(float(9.0))
    elif (20 <= i < 25):
        second_drink.append(float(i - 20.0))
        second_food.append(float(20.0))
    elif (25 <= i < 40):
        second_drink.append(float(i - 25.0))
        second_food.append(float(25.0))
    else:
        second_drink.append(float(i - 40.0))
        second_food.append(float(40.0))

df_secondD = pd.DataFrame(second_drink, columns=['second_drink'])
df_secondF = pd.DataFrame(second_food, columns=['second_food'])

Mydataframe['SECOND_DRINK'] = df_secondD.loc[0: 36500]
Mydataframe['SECOND_FOOD'] = df_secondF.loc[0: 36500]

third_drink = []
third_food = []
for i in thirdC:
    if (10.0>i == 0.0):
        third_drink.append(float(i))
        third_food.append(float(i))
    elif (i == 10.0):
        third_drink.append(float(i - 10.0))
        third_food.append(float(i))
    elif (15 > i > 10):
        third_drink.append(float(i - 10.0))
        third_food.append(float(10.0))
    else:
        third_drink.append(float(i - 15.0))
        third_food.append(float(15.0))

df_thirdD = pd.DataFrame(third_drink, columns=['third_drink'])
df_thirdF = pd.DataFrame(third_food, columns=['third_food'])

Mydataframe['THIRD_DRINK'] = df_thirdD.loc[0: 36500]
Mydataframe['THIRD_FOOD'] = df_thirdF.loc[0: 36500]

Mydataframe.to_csv(input1, index=False)
print(Mydataframe)