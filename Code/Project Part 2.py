import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import os
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import scipy.stats
from sklearn.cluster import KMeans




input1 = "../Data/part1.csv"

Mydataframe = pd.read_csv(input1, sep=",")



datakmeans=pd.DataFrame(Mydataframe[['FIRST_COURSE','SECOND_COURSE','THIRD_COURSE']])


km=KMeans(n_clusters=4)
km.fit(datakmeans)



y_predicted=km.predict(datakmeans)
print(y_predicted)




Mydataframe['LABEL']=y_predicted




Mydataframe.to_csv('../Data/part1.csv')




km.cluster_centers_




fig1 = plt.figure(figsize=(4, 3))
#ax1 = Axes3D(fig1, auto_add_to_figure=False, elev=48, azim=134)
ax1=fig1.add_subplot(111,projection='3d')
dfcluster0=Mydataframe[Mydataframe['LABEL']==0]
dfcluster1=Mydataframe[Mydataframe['LABEL']==1]
dfcluster2=Mydataframe[Mydataframe['LABEL']==2]
dfcluster3=Mydataframe[Mydataframe['LABEL']==3]
ax1.scatter(dfcluster0['FIRST_COURSE'],dfcluster0['SECOND_COURSE'],dfcluster0['THIRD_COURSE'],color='green')
ax1.scatter(dfcluster1['FIRST_COURSE'],dfcluster1['SECOND_COURSE'],dfcluster1['THIRD_COURSE'],color='blue')
ax1.scatter(dfcluster2['FIRST_COURSE'],dfcluster2['SECOND_COURSE'],dfcluster2['THIRD_COURSE'],color='red')
ax1.scatter(dfcluster3['FIRST_COURSE'],dfcluster3['SECOND_COURSE'],dfcluster3['THIRD_COURSE'],color='orange')

ax1.set_xlabel('FIRST_COURSE')
ax1.set_ylabel('SECOND_COURSE')
ax1.set_zlabel('THIRD_COURSE')
ax1.set_title('4 clusters')
ax1.dist = 12
ax1.scatter(km.cluster_centers_[:,0],km.cluster_centers_[:,1],km.cluster_centers_[:,2],s=80,color='black',marker='*')
plt.savefig(os.path.abspath('../Results/Kmeans graph.png'))
plt.show()
